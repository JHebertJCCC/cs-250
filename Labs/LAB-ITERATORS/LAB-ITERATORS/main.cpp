#include <iostream>
#include <fstream>
#include <list>
#include <string>
using namespace std;

#include "Menu.hpp"

list<string> LoadBook( const string& filename );
void ReadBook( list<string> bookText );

int main()
{
    vector<string> books = { "aesop.txt", "fairytales.txt" };

    bool done = false;
    while ( !done )
    {
        Menu::Header( "LIBRARY" );

        cout << "Which book do you want to read?" << endl;
        int choice = Menu::ShowIntMenuWithPrompt( books );

        list<string> bookText = LoadBook( books[choice-1] );
        ReadBook( bookText );
    }

    return 0;
}


list<string> LoadBook( const string& filename )
{
    list<string> bookText;

    cout << "Loading " << filename << "..." << endl;

    ifstream input( filename );

    if ( !input.good() )
    {
        cout << "Error opening file" << endl;
    }

    string line;
    while ( getline( input, line ) )
    {
        bookText.push_back( line );
    }

    cout << endl << bookText.size() << " lines loaded" << endl << endl;

    input.close();

    return bookText;
}


void ReadBook( list<string> bookText )
{
	int pageHeight = 30;
	int counter = 0;
	int totalLines = 0;
	list<string>::iterator it;
	for (it = bookText.begin(); it != bookText.end(); it++) {
		cout << *it << endl;
		totalLines++;
		counter++;

		if (counter == pageHeight) {
			//display menu
			cout << "Line " << totalLines << " of " << bookText.size() << endl << endl;

			int choice = Menu::ShowIntMenuWithPrompt({"BACKWARD","FORWARD"}, false);
			Menu::ClearScreen();

			if (choice == 1) {
				//move iterator back pageLength*2 spaces
				for (int i = 0; i < pageHeight * 2; i++) {
					if (it != bookText.begin()) {
						totalLines--;
						it--;
					}
				}
			}

			counter = 0; //reset
		}
	}
}
