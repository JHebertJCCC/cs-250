#ifndef _function3
#define _function3

bool IsOverdrawn( float balance )
{
    if ( balance <= 0 )
    {
        return false;
    }
    else
    {
        return true;
    }
}

void Test_IsOverdrawn()
{
	cout << "************ Test_IsOverdrawn ************" << endl << endl;

    float input;
    bool expectedOutput;
    bool actualOutput;

    /* TEST 1 ********************************************/
    input = 0;
    expectedOutput = false;

    actualOutput = IsOverdrawn( input );
    if ( actualOutput == expectedOutput )
    {
		cout << "Test_IsOverdrawn: Test 1 passed!" << endl;
    }
    else
    {
        cout << "Test_IsOverdrawn: Test 1 FAILED! \n\t"
        << "Input: " << input << "\n\t"
        << "Expected: " << expectedOutput << "\n\t"
        << "Actual: " << actualOutput << endl << endl;
    }

    /* TEST 2 ********************************************/
    // CREATE YOUR OWN TEST
    input = 12;               
    expectedOutput = true;     


    // Run test (keep this as-is):
    actualOutput = IsOverdrawn( input );
    if ( actualOutput == expectedOutput )
    {
        cout << "Test_IsOverdrawn: Test 2 passed!" << endl;
    }
    else
    {
        cout << "Test_IsOverdrawn: Test 2 FAILED! \n\t"
        << "Input: " << input << "\n\t"
        << "Expected: " << expectedOutput << "\n\t"
        << "Actual: " << actualOutput << endl << endl;
    }

    /* TEST 3 ********************************************/
    // CREATE YOUR OWN TEST
    input = -7;               
    expectedOutput = false;    


    // Run test (keep this as-is):
    actualOutput = IsOverdrawn( input );
    if ( actualOutput == expectedOutput )
    {
        cout << "Test_IsOverdrawn: Test 3 passed!" << endl;
    }
    else
    {
        cout << "Test_IsOverdrawn: Test 3 FAILED! \n\t"
        << "Input: " << input << "\n\t"
        << "Expected: " << expectedOutput << "\n\t"
        << "Actual: " << actualOutput << endl << endl;
    }
}

#endif

