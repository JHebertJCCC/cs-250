#ifndef _PROCESSOR
#define _PROCESSOR

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
using namespace std;

#include "Job.hpp"
#include "Queue.hpp"

class Processor
{
    public:
    void FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile );
    void RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile );
};

void Processor::FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile )
{
	ofstream output(logFile);
	output << "First Come First Searved (FCFS)" << endl;

	int cycleCounter = 0;

	output << "Processing Job #" << jobQueue.Front()->id << endl;
	while (jobQueue.Size() != 0) {
	
		output << "Cycle: " << cycleCounter << "\tRemaining: " << jobQueue.Front()->fcfs_timeRemaining << endl;

		jobQueue.Front()->Work(FCFS);

		if (jobQueue.Front()->fcfs_done == true) {
			jobQueue.Front()->SetFinishTime(cycleCounter, FCFS);
			jobQueue.Pop();
			if (jobQueue.Size() != 0) {
				output << endl;
				output << "Processing Job #" << jobQueue.Front()->id << endl;
			}
		}

		cycleCounter++;

	}
	output << endl;
	int total = 0;
	output << "Job ID" << "\tTime to Completion" << endl;

	for (int i = 0; i < allJobs.size(); i++) {
		output << allJobs[i].id << "\t" << allJobs[i].fcfs_finishTime << endl;
		total += allJobs[i].fcfs_finishTime;
	}

	output << endl;
	output << "Average Completion Time: . . . . ." << total/allJobs.size() << endl;
	output << "\t(Average Time to Complete all Jobs)" << endl;
	output << endl;
	output << "Total Processing Time: . . . . ." << cycleCounter << endl;
	output << "\t(Total Time to Complete all Jobs)" << endl;
	output.close();
}

void Processor::RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile )
{
	ofstream output(logFile);
	output << "Round Robin (RR)" << endl;

	int cycleCounter = 0;
	int timer = 0;

	output << "Processing Job #" << jobQueue.Front()->id << endl;
	while (jobQueue.Size() != 0) {

		if (timer == timePerProcess) {
			jobQueue.Front()->rr_timesInterrupted++;
			jobQueue.Push(jobQueue.Front());
			jobQueue.Pop();
			if (jobQueue.Size() != 0) {
				output << endl;
				output << "Processing Job #" << jobQueue.Front()->id << endl;
			}
			timer = 0;
		}

		output << "Cycle: " << cycleCounter << "\tRemaining: " << jobQueue.Front()->rr_timeRemaining << endl;

		jobQueue.Front()->Work(RR);

		if (jobQueue.Front()->rr_done == true) {
			jobQueue.Front()->SetFinishTime(cycleCounter, RR);
			jobQueue.Pop();
		}

		cycleCounter++;
		timer++;

	}
	output << endl;
	int total = 0;
	output << "Job ID" << "\tTime to Completion" << endl;

	for (int i = 0; i < allJobs.size(); i++) {
		output << allJobs[i].id << "\t" << allJobs[i].rr_finishTime << endl;
		total += allJobs[i].rr_finishTime;
	}

	output << endl;
	output << "Average Completion Time: . . . . ." << total / allJobs.size() << endl;
	output << "\t(Average Time to Complete all Jobs)" << endl;
	output << endl;
	output << "Total Processing Time: . . . . ." << cycleCounter << endl;
	output << "\t(Total Time to Complete all Jobs)" << endl;
	output << endl;
	output << "Round Robin Interval: . . . . ." << timePerProcess << endl;
	output.close();
}

#endif
