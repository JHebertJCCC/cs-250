#ifndef _STACK_HPP
#define _STACK_HPP

#include "Node.hpp"
template <typename T>
class LinkedStack
{
    public:
    LinkedStack() 
    {
    }

    void Push( const T& newData )
    {
		Node<T>* newNode = new Node<T>;
		newNode->data = newData;

		if (m_ptrLast == nullptr)
		{
			m_ptrFirst = newNode;
			m_ptrLast = newNode;
		}

		else
		{
			m_ptrLast->ptrNext = newNode;
			newNode->ptrPrev = m_ptrLast;
			m_ptrLast = newNode;
		}
		m_itemCount++;
    }

    T& Top()
    {

		if (m_ptrLast == nullptr)
		{
			throw out_of_range("No Values, Empty List");
		}

		return m_ptrLast->data;
    }

    void Pop()
    {
		if (m_ptrFirst == nullptr)
		{
			return;
		}
		else if (m_ptrFirst == m_ptrLast)
		{
			delete m_ptrFirst;
			m_ptrFirst = nullptr;
			m_ptrLast = nullptr;
		}
		else
		{
			
			Node<T>* ptrSecondToLast = m_ptrLast->ptrPrev;

			delete m_ptrLast;

			m_ptrLast = ptrSecondToLast;

			m_ptrLast->ptrNext = nullptr;
		}
		m_itemCount--;
    }

    int Size()
    {
		return m_itemCount;
    }

    private:
    Node<T>* m_ptrFirst;
    Node<T>* m_ptrLast;
    int m_itemCount;
};

#endif
