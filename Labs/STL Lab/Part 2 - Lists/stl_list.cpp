// Lab - Standard Template Library - Part 2 - Lists
// Jolie, Hebert
//note: spaces do not work (EX: New York)

#include <iostream>
#include <list>
#include <string>
using namespace std;

int main()
{
	bool done = false;
	list <string> states;
	string newState;
	while (done != true) {
		cout << "----------------------------" << endl;
		cout << "State list size: " << states.size() << endl;
		cout << endl;

		cout << "1. Add new states to front of list "
			<< "2. Add new states to back of list \t" << endl;
		cout << "3. Pop state from front of list "
			<< "4. Pop state from end of list \t" << endl;
		cout << "5. Continue " << endl;
		cout << endl;

		cout << ">> ";
		int choice = 0;
		cin >> choice;
		cout << endl;

		if (choice == 1) {
			cout << "ADD STATE TO FRONT" << endl;
			cout << "Enter new state name: ";
			cin >> newState;
			states.push_front(newState);
			cout << endl;
		}

		else if (choice == 2) {
			cout << "ADD STATE TO BACK" << endl;
			cout << "Enter new state name: ";
			cin >> newState;
			states.push_back(newState);
			cout << endl;
		}

		else if (choice == 3) {
			cout << "REMOVE STATE FROM FRONT" << endl;
			cout << states.front() << " removed" << endl;
			states.pop_front();
			cout << endl;
		}

		else if (choice == 4) {
			cout << "REMOVE STATE FROM BACK" << endl;
			cout << states.back() << " removed" << endl;
			states.pop_back();
			cout << endl;
		}

		else if (choice == 5) {
			//--------------------- normal list
			cout << "Regular List: " << endl;
			for (list<string>::iterator it = states.begin(); it != states.end(); it++)
			{
				cout << *it << "\t";
			}
			cout << endl;

			//----------------------- reversed list
			cout << "Reversed List: " << endl;
			states.reverse();
			for (list<string>::iterator it = states.begin(); it != states.end(); it++)
			{
				cout << *it << "\t";
			}
			cout << endl;

			//----------------------- sorted list
			cout << "Sorted List: " << endl;
			states.sort();
			for (list<string>::iterator it = states.begin(); it != states.end(); it++)
			{
				cout << *it << "\t";
			}
			cout << endl;

			//----------------------- reversed sorted list
			cout << "Reversed Sorted List: " << endl;
			states.reverse();
			for (list<string>::iterator it = states.begin(); it != states.end(); it++)
			{
				cout << *it << "\t";
			}
			cout << endl;

			done = true;
		}
	}

    cin.ignore();
    cin.get();
    return 0;
}
