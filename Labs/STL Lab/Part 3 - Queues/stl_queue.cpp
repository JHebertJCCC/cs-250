// Lab - Standard Template Library - Part 3 - Queues
// Jolie, Hebert

#include <iostream>
#include <string>
#include <queue>
using namespace std;

int main()
{
	queue<float> transactions;
	bool done = false;
	while (done != true) {
		cout << "-------------------------" << endl;
		cout << "Transactions queued: " << transactions.size() << endl;
		cout << endl;
		cout << "1. Enqueue transaction \t"
			<< "2. Continue" << endl;
		cout << endl;
		cout << ">> ";
		int choice;
		cin >> choice;
		cout << endl;

		if (choice == 1) {
			cout << "Enter amount (positive or negative) for next transaction" << endl;
			cout << "\t : ";
			float amount;
			cin >> amount;
			transactions.push(amount);
			cout << endl;
		}
		else if (choice == 2) {
			float accountBalance = 0;
			bool noTransactions = transactions.empty();
			if (noTransactions == false) {
				for (int i = 0; i < transactions.size(); i++) {
					cout << transactions.back() << " pushed to account" << endl;
					accountBalance += transactions.back();
					transactions.pop();
				}
				cout << endl;
				cout << "Final balance: " << accountBalance;
				done = true;
			}
			else {
				cout << "No transactions" << endl;
				done = true;
			}
		}
	}

    cin.ignore();
    cin.get();
    return 0;
}
